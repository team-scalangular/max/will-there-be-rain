import { RuntimeChecks } from '@ngrx/store';

export const runtimeChecksConfig: RuntimeChecks = {
  strictStateImmutability: true,
  strictActionImmutability: true,
  strictActionSerializability: false,
  strictStateSerializability: true,
  strictActionWithinNgZone: true,
  strictActionTypeUniqueness: true,
};
