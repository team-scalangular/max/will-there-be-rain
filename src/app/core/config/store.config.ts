import { NavigationActionTiming, RouterState } from '@ngrx/router-store';
import { metaReducers } from '@core/store';
import { runtimeChecksConfig } from '@core/config/runtime-check.config';

export const mainStoreConfig = {
  metaReducers,
  runtimeChecks: runtimeChecksConfig,
};

export const routerStoreConfig = {
  stateKey: 'router',
  routerState: RouterState.Minimal,
  navigationActionTiming: NavigationActionTiming.PostActivation,
};
