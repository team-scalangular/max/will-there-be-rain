import { createReducer, on } from '@ngrx/store';
import { CallState, LoadingState } from '@shared/interfaces/call-state';
import { AuthActions } from '@core/store/auth/auth-types';

export interface AuthState {
  isLoggedIn: boolean;
  callState: CallState;
}

export const initialState: AuthState = {
  isLoggedIn: false,
  callState: LoadingState.INIT,
};

export const authReducer = createReducer(
  initialState,
  on(AuthActions.signUpRequest, (state) => {
    return {
      ...state,
      callState: LoadingState.LOADING,
    };
  }),
  on(AuthActions.loginRequest, (state) => {
    return {
      ...state,
      callState: LoadingState.LOADING,
    };
  }),
  on(AuthActions.loginSuccess, (state, action) => {
    return {
      ...state,
      callState: LoadingState.LOADED,
      isLoggedIn: true,
    };
  }),
  on(AuthActions.loginError, (state, action) => {
    return {
      ...state,
      callState: {
        errorMessage: action.errorMessage || 'Internal Server Error',
      },
    };
  }),
  on(AuthActions.logout, () => {
    return initialState;
  })
);
