import { createSelector } from '@ngrx/store';
import { getAuthState } from '@core/store';
import { getError, LoadingState } from '@shared/interfaces/call-state';

export const selectIsLoggedIn = createSelector(
  getAuthState,
  (state) => state.isLoggedIn
);

export const selectIsLoading = createSelector(
  getAuthState,
  (state) => state.callState === LoadingState.LOADING
);

export const selectError = createSelector(getAuthState, (state) =>
  getError(state.callState)
);
