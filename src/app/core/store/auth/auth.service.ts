import { Injectable } from '@angular/core';
import {
  Actions,
  createEffect,
  ofType,
  ROOT_EFFECTS_INIT,
} from '@ngrx/effects';
import { AuthActions } from '@core/store/auth/auth-types';
import { catchError, filter, first, map, switchMap, tap } from 'rxjs/operators';
import { AngularFireAuth } from '@angular/fire/auth';
import { User } from '@shared/interfaces/user';
import { BehaviorSubject, from, Observable, of } from 'rxjs';
import { Store } from '@ngrx/store';
import { CoreState } from '@core/store';
import {
  selectError,
  selectIsLoading,
  selectIsLoggedIn,
} from '@core/store/auth/auth.selectors';
import firebase from 'firebase';
import { AuthInfos } from '@shared/interfaces/auth-infos';
import { AngularFirestore } from '@angular/fire/firestore';
import { HttpClient } from '@angular/common/http';
import { environment } from '@env/environment';
import UserCredential = firebase.auth.UserCredential;

@Injectable()
export class AuthService {
  // ************************************************
  // Observable Queries available for consumption by views
  // ************************************************

  // not possible to save firebase User object into store
  firebaseUser$: BehaviorSubject<firebase.User> = new BehaviorSubject<firebase.User>(
    undefined
  );

  // TODO User reducer: user$: Observable<User> = (make user service???);
  loading$: Observable<boolean> = this.store.select(selectIsLoading);
  error$: Observable<string> = this.store.select(selectError);
  isLoggedIn$: Observable<boolean> = this.store.select(selectIsLoggedIn);

  // ************************************************
  // Effects to be registered at the Module level
  // ************************************************

  // get current user from firebase
  getUser$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AuthActions.getFirebaseUser, ROOT_EFFECTS_INIT),
      switchMap(() => this.angularFireAuth.authState),
      filter((fireBaseUser) => !!fireBaseUser),
      first(),
      map((firebaseUser) => {
        this.firebaseUser$.next(firebaseUser);
        return AuthActions.loginSuccess({ firebaseUid: firebaseUser.uid });
      }),
      catchError((error) =>
        of(AuthActions.loginError(error.message || 'Unauthorized'))
      )
    );
  });

  handleLoginSuccess$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(AuthActions.loginSuccess),
        map((action) => action.firebaseUid),
        tap(() => {
          // TODO go to User reducer and store User
        })
      );
    },
    { dispatch: false }
  );

  // signUpEffect
  signUp$ = createEffect(() => {
    let userAuthInfos: Partial<AuthInfos>;
    return this.actions$.pipe(
      ofType(AuthActions.signUpRequest),
      map((action) => action.authInfos),
      switchMap((authInfos) => {
        userAuthInfos = authInfos;
        return from(this.signUpToFirebase(authInfos.email, authInfos.password));
      }),
      switchMap((userCredentials: UserCredential) => {
        userCredentials.user.sendEmailVerification();

        return this.createNewUser({
          displayName: userAuthInfos.alias,
          firebaseUid: userCredentials.user.uid,
          tourRefs: [],
        });
      }),
      map(() => {
        return AuthActions.getFirebaseUser();
      }),
      catchError((error) => of(AuthActions.loginError(error.message)))
    );
  });

  login$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AuthActions.loginRequest),
      switchMap((action) => {
        return from(this.signInToFirebase(action.authInfos));
      }),
      map((userCredentials) => {
        return AuthActions.getFirebaseUser();
      }),
      catchError((error) => of(AuthActions.loginError(error.message)))
    );
  });

  constructor(
    private actions$: Actions,
    private angularFireAuth: AngularFireAuth,
    private angularFireStore: AngularFirestore,
    private store: Store<CoreState>,
    private http: HttpClient
  ) {}

  // ******************************************
  // Static
  // ******************************************

  private static setPersistence(rememberMe: boolean): Promise<void> {
    return firebase
      .auth()
      .setPersistence(
        rememberMe
          ? firebase.auth.Auth.Persistence.LOCAL
          : firebase.auth.Auth.Persistence.SESSION
      );
  }

  // ******************************************
  // Service Methods
  // ******************************************

  public signUp(authInfos: AuthInfos): void {
    AuthService.setPersistence(authInfos.rememberMe)
      .then(() => {
        this.store.dispatch(
          AuthActions.signUpRequest({
            authInfos: {
              email: authInfos.email,
              password: authInfos.password,
              alias: authInfos.alias,
            },
          })
        );
      })
      .catch((error) =>
        this.store.dispatch(AuthActions.loginError(error.message))
      );
  }

  public logout(): void {
    this.angularFireAuth
      .signOut()
      .then(() => this.store.dispatch(AuthActions.logout()));
  }

  // Partial typeof {email: string, password: string}
  public login(authInfos: AuthInfos): void {
    AuthService.setPersistence(authInfos.rememberMe)
      .then(() => this.store.dispatch(AuthActions.loginRequest({ authInfos })))
      .catch((error) =>
        this.store.dispatch(AuthActions.loginError(error.message))
      );
  }

  // ******************************************
  // Internal Methods
  // ******************************************

  private signUpToFirebase(
    email: string,
    password: string
  ): Promise<firebase.auth.UserCredential> {
    return this.angularFireAuth.createUserWithEmailAndPassword(email, password);
  }

  private createNewUser(user: User): Observable<User> {
    return this.http.post<User>(
      environment.rootUrl + environment.userAPIPath,
      user
    );
  }

  private signInToFirebase(authInf: AuthInfos): Promise<UserCredential> {
    return this.angularFireAuth.signInWithEmailAndPassword(
      authInf.email,
      authInf.password
    );
  }
}
