import { createAction, props } from '@ngrx/store';
import { AuthInfos } from '@shared/interfaces/auth-infos';

export const notAuthenticated = createAction('[Auth] Not Authenticated');

export const startAuthProcess = createAction('[Auth] Login requested');

export const getFirebaseUser = createAction('[Auth] Get User');

export const signUpRequest = createAction(
  '[Auth] SignUp Requested',
  props<{ authInfos: Partial<AuthInfos> }>()
);

export const loginRequest = createAction(
  '[Auth] Login',
  props<{ authInfos: AuthInfos }>()
);

export const loginSuccess = createAction(
  '[Auth] Login Success',
  props<{ firebaseUid: string }>()
);

export const logout = createAction('[Auth] Logout');

export const loginError = createAction(
  '[Auth] Login Error',
  props<{ errorMessage: string }>()
);
