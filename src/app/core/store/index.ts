import {
  ActionReducerMap,
  createFeatureSelector,
  MetaReducer,
} from '@ngrx/store';
import { environment } from '@env/environment';
import { routerReducer, RouterReducerState } from '@ngrx/router-store';
import { authReducer, AuthState } from '@core/store/auth/auth.reducer';

export interface CoreState {
  router: RouterReducerState;
  auth: AuthState;
}

export const reducers: ActionReducerMap<CoreState> = {
  router: routerReducer,
  auth: authReducer,
};

export const metaReducers: MetaReducer<CoreState>[] = !environment.production
  ? []
  : [];

export const getAuthState = createFeatureSelector<AuthState>('auth');
