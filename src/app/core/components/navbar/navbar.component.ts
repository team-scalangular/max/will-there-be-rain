import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { filter, map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { AuthService } from '@core/store/auth/auth.service';

@Component({
  selector: 'core-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NavbarComponent implements OnInit {
  addedPrimaryBackground: boolean;

  navbarState$: Observable<{
    isLoggedIn: boolean;
  }> = this.authService.isLoggedIn$.pipe(map((isLoggedIn) => ({ isLoggedIn })));

  constructor(private router: Router, private authService: AuthService) {}

  urlsWithTransparentHeader: Array<string> = [
    '/',
    'auth',
    '/auth/sign_up',
    '/auth/login',
  ];

  ngOnInit(): void {
    this.addedPrimaryBackground = false;

    this.router.events
      .pipe(filter((event) => event instanceof NavigationEnd))
      .subscribe((route: NavigationEnd) => {
        if (this.isTransparentUrl(route.url)) {
          document.getElementsByTagName('nav')[0].classList.add('bg-primary');
          this.addedPrimaryBackground = true;
        } else if (this.addedPrimaryBackground) {
          document
            .getElementsByTagName('nav')[0]
            .classList.remove('bg-primary');
          this.addedPrimaryBackground = false;
        }
      });
  }

  private isTransparentUrl(urlToCheck: string): boolean {
    return this.urlsWithTransparentHeader.indexOf(urlToCheck) === -1;
  }

  onLogOut(): void {
    this.authService.logout();
  }
}
