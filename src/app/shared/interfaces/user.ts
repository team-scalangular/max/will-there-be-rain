export interface User {
  uid?: string;
  __v?: number;
  firebaseUid: string;
  displayName: string;
  tourRefs: string[];
  photo?: Blob;
}
