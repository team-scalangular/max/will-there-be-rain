export interface AuthInfos {
  rememberMe: boolean;
  alias?: string;
  email: string;
  password: string;
}
