import { User } from '@shared/interfaces/user';

export interface UserWithAdditions extends User {
  email: string;
  password: string;
  rememberMe: boolean;
}
