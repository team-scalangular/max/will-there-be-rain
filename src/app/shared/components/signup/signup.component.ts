import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { AuthService } from '@core/store/auth/auth.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SignupComponent implements OnInit {
  signUpForm: FormGroup;

  /*
   getters for form controls
   */
  get formControlEmail(): AbstractControl {
    return this.signUpForm.get('email');
  }

  get formControlPassword(): AbstractControl {
    return this.signUpForm.get('password');
  }

  get formControlAlias(): AbstractControl {
    return this.signUpForm.get('alias');
  }

  get formControlRememberMe(): AbstractControl {
    return this.signUpForm.get('rememberMe');
  }

  constructor(private authService: AuthService) {}

  ngOnInit(): void {
    this.signUpForm = new FormGroup({
      email: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
      alias: new FormControl('', [
        Validators.required,
        Validators.minLength(3),
      ]),
      rememberMe: new FormControl(''),
    });
  }

  onSignUp(): void {
    this.authService.signUp({
      email: this.formControlEmail.value,
      password: this.formControlPassword.value,
      alias: this.formControlAlias.value,
      rememberMe: this.formControlRememberMe.value,
    });
  }
}
