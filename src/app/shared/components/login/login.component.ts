import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup } from '@angular/forms';
import { AuthService } from '@core/store/auth/auth.service';

@Component({
  selector: 'shared-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;

  /*
   getters for form control
   */
  get loginFormEmail(): AbstractControl {
    return this.loginForm.get('email');
  }

  get loginFormPassword(): AbstractControl {
    return this.loginForm.get('password');
  }

  get loginFormRememberMe(): AbstractControl {
    return this.loginForm.get('rememberMe');
  }

  constructor(private authService: AuthService) {}

  ngOnInit(): void {
    this.loginForm = new FormGroup({
      email: new FormControl(''),
      password: new FormControl(''),
      rememberMe: new FormControl(false),
    });
    this.authService.firebaseUser$.subscribe((firebaseUser) => {
      console.log(firebaseUser);
    });
  }

  onLogin(): void {
    this.authService.login({
      email: this.loginFormEmail.value,
      password: this.loginFormPassword.value,
      rememberMe: this.loginFormRememberMe.value,
    });
  }
}
