import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { TourDashboardRouting } from './tour-dashboard.routing';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { SharedModule } from '@shared/shared.module';
import { AuthComponent } from './auth/auth.component';
import { TourDashboardComponent } from './tour-dashboard/tour-dashboard.component';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [HomeComponent, AuthComponent, TourDashboardComponent],
  imports: [
    CommonModule,
    TourDashboardRouting,
    MDBBootstrapModule,
    SharedModule,
    LeafletModule,
    ReactiveFormsModule,
  ],
})
export class TourDashboardModule {}
