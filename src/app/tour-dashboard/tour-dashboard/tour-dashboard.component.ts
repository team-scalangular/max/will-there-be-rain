import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { icon, latLng, Layer, MapOptions, marker, tileLayer } from 'leaflet';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-tour-dashboard',
  templateUrl: './tour-dashboard.component.html',
  styleUrls: ['./tour-dashboard.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TourDashboardComponent implements OnInit {
  tourForm: FormGroup;

  options: MapOptions = {
    layers: [
      tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 18,
        attribution: 'Will-there-be-rain',
      }),
    ],
    zoom: 14,
    center: latLng(52.39886, 13.06566),
  };

  layers: Layer[] = [
    marker([52.39886, 13.06566], {
      icon: icon({
        iconSize: [25, 41],
        iconAnchor: [13, 41],
        iconUrl: 'assets/marker-icon.png',
        shadowUrl: 'assets/marker-shadow.png',
      }),
    }),
  ];

  constructor() {}

  ngOnInit(): void {
    this.initializeTourForm();
  }

  private initializeTourForm(): void {
    this.tourForm = new FormGroup({
      start: new FormControl('', [
        Validators.required,
        Validators.minLength(4),
      ]),
      destination: new FormControl('', [
        Validators.required,
        Validators.minLength(4),
      ]),
    });
  }

  onSubmitRoute(): void {
    console.log('Submit');
  }
}
