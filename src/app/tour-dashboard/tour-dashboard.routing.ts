import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { NgModule } from '@angular/core';
import { SignupComponent } from '@shared/components/signup/signup.component';
import { AuthComponent } from '@tour-dashboard/auth/auth.component';
import { LoginComponent } from '@shared/components/login/login.component';
import { TourDashboardComponent } from '@tour-dashboard/tour-dashboard/tour-dashboard.component';

export const routes: Routes = [
  { path: '', component: HomeComponent },
  {
    path: 'auth',
    component: AuthComponent,
    children: [
      { path: '', redirectTo: '/auth/sign_up', pathMatch: 'full' },
      { path: 'login', component: LoginComponent },
      { path: 'sign_up', component: SignupComponent },
    ],
  },
  { path: 'tour-dashboard', component: TourDashboardComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TourDashboardRouting {}
