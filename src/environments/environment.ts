// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyB0VZqcCBgwRcfbpobu0upol653Yj74cM4',
    authDomain: 'willthereberain-dev.firebaseapp.com',
    databaseURL: 'https://willthereberain-dev.firebaseio.com',
    projectId: 'willthereberain-dev',
    storageBucket: 'willthereberain-dev.appspot.com',
    messagingSenderId: '670135233465',
    appId: '1:670135233465:web:738775f3972faf254da468',
    measurementId: 'G-T9BH4JM5XC',
  },
  rootUrl: 'http://localhost:8080',
  userAPIPath: '/users',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
