export const environment = {
  production: true,
  firebaseConfig: {
    apiKey: 'AIzaSyBqoGHEjLOx3kPy0mtEqbYMoOg11k92qdI',
    authDomain: 'willthereberain.firebaseapp.com',
    databaseURL: 'https://willthereberain.firebaseio.com',
    projectId: 'willthereberain',
    storageBucket: 'willthereberain.appspot.com',
    messagingSenderId: '686523217419',
    appId: '1:686523217419:web:cb70d212dea84ca1da790e',
    measurementId: 'G-Y0SVJ01L5D',
  },
  rootUrl: 'http://localhost:8080',
  userAPIPath: '/users',
};
